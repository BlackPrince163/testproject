package com.company;

import java.util.Scanner;

public class TicTacToeSave {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Напиши имя первого игрока");
        String player1 = scanner.next();
        System.out.println("Напиши имя второго игрока");
        String player2 = scanner.next();
        String[][] table = new String[3][3];
        for (int i = 0; i < 3; i++) {
            System.out.println();
            for (int j = 0; j < 3; j++) {
                table[i][j] = (" [ ] ");
                System.out.print(table[i][j]);
            }
        }
        System.out.println();
        int i = 2;
        int quit = 0;

        while ((i < 11) & (quit == 0)) {


            if (i % 2 == 0) {
                System.out.println();
                System.out.println("Твой ход " + player1);
            } else {
                System.out.println();
                System.out.println("Ваш ход " + player2);
            }
            System.out.print("Введите клетку: ");
            String key = scanner.next();
            switch (key) {
                case "1": {
                    if (table[0][0] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[0][0] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[0][0] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "2": {
                    if (table[0][1] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[0][1] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[0][1] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "3": {
                    if (table[0][2] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[0][2] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[0][2] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "4": {
                    if (table[1][0] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[1][0] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[1][0] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "5": {
                    if (table[1][1] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[1][1] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[1][1] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "6": {
                    if (table[1][2] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[1][2] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[1][2] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "7": {
                    if (table[2][0] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[2][0] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[2][0] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "8": {
                    if (table[2][1] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[2][1] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[2][1] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                case "9": {
                    if (table[2][2] == (" [ ] ")) {
                        if (i % 2 == 0) {
                            table[2][2] = (" [X] ");
                            i++;
                            break;
                        } else {
                            table[2][2] = (" [O] ");
                            i++;
                            break;
                        }
                    } else {
                        System.out.println("Занято, введите другую");
                        break;
                    }
                }
                default:
                    System.out.println("Ошибка");
            }
            for (int t = 0; t < 3; t++) {
                System.out.println();
                for (int p = 0; p < 3; p++) {
                    System.out.print(table[t][p]);
                }
            }
            System.out.println();
            for (int t = 0; t<3 ; t++){
                if      (((table[t][0]) == " [O] " & (table[t][1]) == " [O] " & table[t][2] == " [O] ") ||
                        ((table[0][t]) == " [O] " & (table[1][t]) == " [O] " & table[2][t] == " [O] ")) {
                    System.out.print("Победил   " + player2);
                    quit++;
                }
                else if (((table[t][0]) == " [X] " & (table[t][1]) == " [X] " & table[t][2] == " [X] ") ||
                        ((table[0][t]) == " [X] " & (table[1][t]) == " [X] " & table[2][t] == " [X] ")) {
                    System.out.print("Победил  " + player1);
                    quit++;
                }
                else {}
            }
            if      (((table[0][0]) == " [X] " & (table[1][1]) == " [X] " & table[2][2] == " [X] ") ||
                    ((table[0][2]) == " [X] " & (table[1][1]) == " [X] " & table[2][0] == " [X] ")) {
                System.out.print("Победил  " + player1);
                quit++;
            }
            else if (((table[0][0]) == " [O] " & (table[1][1]) == " [O] " & table[2][2] == " [O] ") ||
                    ((table[0][2]) == " [O] " & (table[1][1]) == " [O] " & table[2][0] == " [O] ")) {
                System.out.print("Победил  " + player2);
                quit++;
            }

        }
        if (quit == 0) {
            System.out.println("Ничья");
        }
        System.out.println();
    }

}

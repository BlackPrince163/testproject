package com.company;


import java.util.Scanner;

public class SwitchCase {
    public static void main(String[] args) {
        System.out.println("Number month");
        Scanner scanner = new Scanner(System.in);
        int NumMonth = scanner.nextInt();
        String output;
        switch (NumMonth) {
            case 1:
                output = "January";
                break;
            case 2:
                output = "February";
                break;
            case 3:
                output = "March";
                break;
            case 4:
                output = "April";
                break;
            case 5:
                output = "May";
                break;
            case 6:
                output = "June";
                break;
            case 7:
                output = "Jule";
                break;
            case 8:
                output = "August";
                break;
            case 9:
                output = "September";
                break;
            case 10:
                output = "October";
                break;
            case 11:
                output = "November";
                break;
            case 12:
                output = "December";
                break;
            default:
                output = "No";
                break;
        }
        System.out.println(output);
    }
}
